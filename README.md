# Carry

Carry is a digital platform to help bridge the information gap between Cognitive Behavioral Therapists and their patients.

View our pitch presentation [here](screenshots/carry-presentation.pdf).

Mobile App: http://www.carrycompanion.com:8100

Web App: http://www.carrycompanion.com

API Documentation: http://api.carrycompanion.com/swagger-ui.html

![](screenshots/Deployment.PNG "Title")

# UI

The UI is built with React/Typescript and the Ionic Framework.

For more information and to learn build locally please look [here](./ui/README.md)

# Services

The Services are built with Java/SpringBoot.

For more information and to learn build locally please look [here](./services/README.md)

# Screen Shots

<img src="screenshots/ios1.png"  width="200" height="400">
<img src="screenshots/ios2.png"  width="200" height="400">
<img src="screenshots/ios3.png"  width="200" height="400">

<br />
<img src="screenshots/android1.png"  width="200" height="400">
<img src="screenshots/android2.png"  width="200" height="400">
<img src="screenshots/android3.png"  width="200" height="400">

<br />
<img src="screenshots/desktop.png"
     alt="View7"
     style="width: 70%" />
