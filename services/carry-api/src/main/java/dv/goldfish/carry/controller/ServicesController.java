package dv.goldfish.carry.controller;

import dv.goldfish.carry.dao.ClientDao;
import dv.goldfish.carry.model.ClientData;
import dv.goldfish.carry.model.GoogleCalendarUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController()
@RequestMapping("/api/services")
public class ServicesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientDao clientDao;

    @RequestMapping(value = "/google/calendar/client", method = RequestMethod.GET)
    public List<ClientData> getClientData(){
        return clientDao.getAll();
    }

    @RequestMapping(value = "/google/calendar/client/{id}", method = RequestMethod.POST)
    public void postCalendarUpdate(@PathVariable String id, @RequestBody List<GoogleCalendarUpdate> googleCalendarUpdate) {
        LOGGER.info("id {}, {}", id, googleCalendarUpdate);
        clientDao.addGoogleCalendarUpdate(id, googleCalendarUpdate);
    }

    @RequestMapping(value = "/google/calendar/client/{id}", method = RequestMethod.GET)
    public List<GoogleCalendarUpdate> getCalendarUpdate(@PathVariable String id) {
        return clientDao.getGoogleCalendarUpdate(id);
    }

}
