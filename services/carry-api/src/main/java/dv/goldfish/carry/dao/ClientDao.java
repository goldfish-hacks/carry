package dv.goldfish.carry.dao;

import dv.goldfish.carry.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClientDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    public void save(ClientData clientData){
        mongoTemplate.save(clientData);
    }

    public List<ClientData> getAll(){
        return mongoTemplate.findAll(ClientData.class);
    }

    public ClientData get(String id){
        return getClientData(id);
    }

    public List<PushNotification> getPushNotification(String id) {
        return getClientData(id).getPushNotifications();
    }

    public void addPushNotification(String id, List<PushNotification> pushNotifications) {
        Query query = Query.query(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("pushNotifications", pushNotifications);
        mongoTemplate.findAndModify(query, update, ClientData.class);
    }

    public List<GoogleCalendarUpdate> getGoogleCalendarUpdate(String id) {
        return getClientData(id).getGoogleCalendarUpdates();
    }

    public void addGoogleCalendarUpdate(String id, List<GoogleCalendarUpdate> googleCalendarUpdate) {
        Query query = Query.query(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("googleCalendarUpdates", googleCalendarUpdate);
        mongoTemplate.findAndModify(query, update, ClientData.class);
    }

    public List<Mindset> getMindset(String id) {
        return getClientData(id).getMindsets();
    }

    public void addMindset(String id, Mindset mindset){
        Query query = Query.query(Criteria.where("id").is(id));
        Update update = new Update();
        update.push("mindsets", mindset);
        mongoTemplate.findAndModify(query, update, ClientData.class);
    }

    private ClientData getClientData(String id) {
        Query query = Query.query(Criteria.where("_id").is(id));
        ClientData clientData = mongoTemplate.findOne(query, ClientData.class);
        return clientData != null ? clientData : new ClientData();
    }

    public List<Insight> getInsight(String id) {
        return getClientData(id).getInsights();
    }
}
