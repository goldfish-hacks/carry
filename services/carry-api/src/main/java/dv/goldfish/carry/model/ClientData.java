package dv.goldfish.carry.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientData {

    private String id;
    private String name;
    private String age;
    private String photo;

    @JsonIgnore
    private List<PushNotification> pushNotifications = new ArrayList<>();
    @JsonIgnore
    private List<GoogleCalendarUpdate> googleCalendarUpdates = new ArrayList<>();
    @JsonIgnore
    private List<Mindset> mindsets = new ArrayList<>();
    @JsonIgnore
    private List<Insight> insights = new ArrayList<>();

    public ClientData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<PushNotification> getPushNotifications() {
        return pushNotifications;
    }

    public void setPushNotifications(List<PushNotification> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

    public List<GoogleCalendarUpdate> getGoogleCalendarUpdates() {
        return googleCalendarUpdates;
    }

    public void setGoogleCalendarUpdates(List<GoogleCalendarUpdate> googleCalendarUpdates) {
        this.googleCalendarUpdates = googleCalendarUpdates;
    }

    public List<Mindset> getMindsets() {
        return mindsets;
    }

    public void setMindsets(List<Mindset> mindsets) {
        this.mindsets = mindsets;
    }

    public List<Insight> getInsights() {
        return insights;
    }

    public void setInsights(List<Insight> insights) {
        this.insights = insights;
    }
}
