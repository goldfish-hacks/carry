package dv.goldfish.carry.controller;

import dv.goldfish.carry.dao.ClientDao;
import dv.goldfish.carry.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/client")
public class ClientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientDao clientDao;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public void addClients(ClientData clientData){
        LOGGER.info("{}", clientData);
        clientDao.save(clientData);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ClientData> getAllClient() {
        return clientDao.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ClientData getClient(String id) {
        return clientDao.get(id);
    }

    @RequestMapping(value = "/{id}/pushnotification", method = RequestMethod.GET)
    public List<PushNotification> postCalendarUpdate(@PathVariable String id) {
        return clientDao.getPushNotification(id);
    }

    @RequestMapping(value = "/{id}/mindset", method = RequestMethod.POST)
    public void postMindset(@PathVariable String id, @RequestBody Mindset mindset) {
        LOGGER.info("id {}, {}", id, mindset);
        clientDao.addMindset(id, mindset);
    }

    @RequestMapping(value = "/{id}/report/{type}", method = RequestMethod.GET)
    public ClientReport getClientReport(@PathVariable String id, @PathVariable String type) {
        return new ClientReport();
    }

    @RequestMapping(value = "/{id}/insight", method = RequestMethod.GET)
    public List<Insight> getInsight(@PathVariable String id) {
        return clientDao.getInsight(id);
    }

}
