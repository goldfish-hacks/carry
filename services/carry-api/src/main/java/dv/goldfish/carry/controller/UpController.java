package dv.goldfish.carry.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpController {

    @RequestMapping("/up")
    public String index() {
        return "Up";
    }

}
