package dv.goldfish.carry.controller;

import dv.goldfish.carry.dao.ClientDao;
import dv.goldfish.carry.model.PushNotification;
import dv.goldfish.carry.model.ClientData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController()
@RequestMapping("/api/algo")
public class AlgoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlgoController.class);

    @Autowired
    private ClientDao clientDao;

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public List<ClientData> getClientData(){
        return clientDao.getAll();
    }

    @RequestMapping(value = "/pushnotification/client/{id}", method = RequestMethod.POST)
    public void postCalendarUpdate(@PathVariable String id, @RequestBody List<PushNotification> pushNotifications) {
        LOGGER.info("id {}, {}", id, pushNotifications);
        clientDao.addPushNotification(id, pushNotifications);
    }

}
