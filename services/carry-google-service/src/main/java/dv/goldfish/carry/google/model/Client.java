package dv.goldfish.carry.google.model;

public class Client {
    private String googleAuthToken;
    private String id;

    public Client() {
    }

    public Client(String googleAuthToken, String id) {
        this.googleAuthToken = googleAuthToken;
        this.id = id;
    }

    public String getGoogleAuthToken() {
        return googleAuthToken;
    }

    public void setGoogleAuthToken(String googleAuthToken) {
        this.googleAuthToken = googleAuthToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
