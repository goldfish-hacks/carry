package dv.goldfish.carry.google.controller;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import dv.goldfish.carry.google.model.Client;
import dv.goldfish.carry.google.model.EventDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import com.google.api.services.calendar.model.Events;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static dv.goldfish.carry.google.controller.CarryService.getClients;
import static dv.goldfish.carry.google.controller.CarryService.saveUpcomingGoogleCalendarEvents;
import static dv.goldfish.carry.google.controller.GoogleCalendarServices.getUpcomingGoogleCalendarEvents;

@Controller
public class GoogleUserCalendarController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleUserCalendarController.class);

    @Scheduled(fixedRate = 60000)
    public static void run(){
        LOGGER.info("Start Google Calendar Refresh");
        List<Client> clientList = getClients();
        for(Client client : clientList) {
            Events upcomingCalendarEvents = null;
            try {
                upcomingCalendarEvents = getUpcomingGoogleCalendarEvents();
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<EventDetail> eventList;
            if (upcomingCalendarEvents != null) {
                eventList = formatCalendarEvents(upcomingCalendarEvents);
                saveUpcomingGoogleCalendarEvents(client, eventList);
            }
        }
        LOGGER.info("End Google Calendar Refresh");
    }

    private static List<EventDetail> formatCalendarEvents(Events upcomingCalendarEvents) {
        List<Event> items = upcomingCalendarEvents.getItems();
        List<EventDetail> eventsMap = new ArrayList<>();
        if (items.isEmpty()) {
            System.out.println("No upcoming events found.");
        } else {
            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    start = event.getStart().getDate();
                }
                DateTime end = event.getEnd().getDateTime();
                if (end == null) {
                    end = event.getEnd().getDate();
                }
                List<EventAttendee> allAttendees = event.getAttendees();
                List<String> allAttendeesList = new ArrayList<>();
                if (allAttendees != null){
                    for(EventAttendee attendee : allAttendees){
                        String name = attendee.getDisplayName();
                        if(name == null || name.equals("")){
                            name = attendee.getEmail();
                        }
                        String response = attendee.getResponseStatus();
                        String attendeeDetail = name + " (" + response + ")";
                        allAttendeesList.add(attendeeDetail);
                    }
                }
                EventDetail detail = new EventDetail(event.getSummary(), start, end, allAttendeesList);
                eventsMap.add(detail);
            }
        }
        return eventsMap;
    }

}
