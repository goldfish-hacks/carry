package dv.goldfish.carry.google.controller;

import dv.goldfish.carry.google.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.*;


class CarryService {

    private static RestTemplate restTemplate = new RestTemplate();
    private static String CARRY_SERVICES = "http://api.carrycompanion.com/api/services/google/calendar/client";

    static void saveUpcomingGoogleCalendarEvents(Client client, List<EventDetail> eventList) {
        String url = CARRY_SERVICES + "/" + client.getId();
        restTemplate.postForObject(url, eventList, ResponseEntity.class);
    }

    static List<Client> getClients() {
        Client[] clientList = restTemplate.getForObject(CARRY_SERVICES, Client[].class);
        return Arrays.asList(Objects.requireNonNull(clientList));
    }

}
