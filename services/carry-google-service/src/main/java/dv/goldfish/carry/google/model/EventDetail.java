package dv.goldfish.carry.google.model;

import com.google.api.client.util.DateTime;

import java.util.Date;
import java.util.List;

public class EventDetail {
    private String eventName;
    private Date startTime;
    private Date endTime;
    private List<String> attendeeList;

    public EventDetail() {
    }

    public EventDetail(String eventName, DateTime startTime, DateTime endTime, List<String> attendeeList) {
        this.eventName = eventName;
        this.startTime = new Date(startTime.getValue());
        this.endTime = new Date(endTime.getValue());
        this.attendeeList = attendeeList;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<String> getAttendeeList() {
        return attendeeList;
    }

    public void setAttendeeList(List<String> attendeeList) {
        this.attendeeList = attendeeList;
    }
}
