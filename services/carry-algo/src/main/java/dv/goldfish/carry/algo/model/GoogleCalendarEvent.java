package dv.goldfish.carry.algo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleCalendarEvent {
    private String eventName;
    private Date startTime;
    private Date endTime;
    private List<String> attendeeList;

    public GoogleCalendarEvent() {
    }

    public GoogleCalendarEvent(String eventName, Date startTime, Date endTime, List<String> attendeeList) {
        this.eventName = eventName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.attendeeList = attendeeList;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public List<String> getAttendeeList() {
        return attendeeList;
    }

    public void setAttendeeList(List<String> attendeeList) {
        this.attendeeList = attendeeList;
    }
}
