package dv.goldfish.carry.algo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AlgoMain {

    public static void main(String[] args) {
        SpringApplication.run(AlgoMain.class, args);
    }
}
