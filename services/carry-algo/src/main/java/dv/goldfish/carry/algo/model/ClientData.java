package dv.goldfish.carry.algo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientData {

    private String id;
    private List<PushNotification> pushNotifications = new ArrayList<>();
    private List<GoogleCalendarEvent> googleCalendarEvents = new ArrayList<>();

    public ClientData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<PushNotification> getPushNotifications() {
        return pushNotifications;
    }

    public void setPushNotifications(List<PushNotification> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

    public List<GoogleCalendarEvent> getGoogleCalendarEvents() {
        return googleCalendarEvents;
    }

    public void setGoogleCalendarEvents(List<GoogleCalendarEvent> googleCalendarEvents) {
        this.googleCalendarEvents = googleCalendarEvents;
    }
}
