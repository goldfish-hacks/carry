package dv.goldfish.carry.algo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PushNotification {
    private Date dateToSend;
    private String type;
    private String id;
    private String message;
    private String reason;

    public PushNotification() {
    }

    public PushNotification(Date dateToSend, String type, String id) {
        this.dateToSend = dateToSend;
        this.type = type;
        this.id = id;
    }

    public PushNotification(Date dateToSend, String type, String id, String message, String reason) {
        this.dateToSend = dateToSend;
        this.type = type;
        this.id = id;
        this.message = message;
        this.reason = reason;
    }

    public Date getDateToSend() {
        return dateToSend;
    }

    public void setDateToSend(Date dateToSend) {
        this.dateToSend = dateToSend;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
