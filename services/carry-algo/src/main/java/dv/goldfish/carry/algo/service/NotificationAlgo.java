package dv.goldfish.carry.algo.service;

import dv.goldfish.carry.algo.model.ClientData;
import dv.goldfish.carry.algo.model.GoogleCalendarEvent;
import dv.goldfish.carry.algo.model.PushNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class NotificationAlgo {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationAlgo.class);

    private RestTemplate restTemplate = new RestTemplate();
    private static final String CARRY_URL = "http://api.carrycompanion.com/api/";
//    private static final String CARRY_URL = "http://localhost:8080/api/";

    @Scheduled(fixedRate = 60000)
    public void run(){
        LOGGER.info("Start Notification Algo");
        List<ClientData> clientDataList = getClientData();
        for(ClientData clientData : clientDataList) {
            List<PushNotification> pushNotificationList = calculateNotifications(clientData);
            saveClientNotification(clientData, pushNotificationList);
        }
        LOGGER.info("End Notification Algo");
    }

    private List<ClientData> getClientData(){
        ClientData[] clientData = restTemplate.getForObject(CARRY_URL + "/client/all", ClientData[].class);
        return Arrays.asList(clientData);
    }

    private List<PushNotification> calculateNotifications(ClientData clientData) {
        //calendar based
        //activity based
        //history based - when do they volunteer/when have they skipped
        //sanitize date - ensure it's not less than 30 min apart and not more that 4 per day

        //determine current time
        //get all calendar events
        //get list of push notifications
        //loop though events
        //see if back to back event/overlapped
        //if yes keep checking until we get to end of meeting block
        //get end time of last meeting and check if there's push notification for that time
        //if yes, skip. if no, add it in correct place in list

        List<GoogleCalendarEvent> googleCalendarEvents = getGoogleCalendarEvents(clientData);
        boolean inEvent = false;
        boolean sendNotification = false;
        Date currentTime = new Date();
        for(GoogleCalendarEvent googleCalendarEvent : googleCalendarEvents){
            Date startTime = googleCalendarEvent.getStartTime();
            Date endTime = googleCalendarEvent.getEndTime();
            if(startTime.before(currentTime) && endTime.after(currentTime)){
                inEvent = true;
                break;
            } else if(currentTime.after(endTime)
                    && Math.abs(currentTime.getTime() - endTime.getTime()) > TimeUnit.MINUTES.toMillis(5)
                    && Math.abs(currentTime.getTime() - endTime.getTime()) < TimeUnit.MINUTES.toMillis(10)
            ){
                sendNotification = true;
            }
        }

        if(!inEvent && sendNotification){
            PushNotification pushNotification = new PushNotification(currentTime, "After Meeting", clientData.getId(), "What is you mindset?", "After Meeting");
            return Collections.singletonList(pushNotification);
        } else {
            return Collections.emptyList();
        }
    }

    private List<GoogleCalendarEvent> getGoogleCalendarEvents(ClientData clientData){
        GoogleCalendarEvent[] googleCalendarEvents = restTemplate.getForObject(CARRY_URL+"/services/google/calendar/client/" + clientData.getId(), GoogleCalendarEvent[].class);
        return Arrays.asList(googleCalendarEvents);
    }

    private void saveClientNotification(ClientData clientData, List<PushNotification> pushNotifications) {
        restTemplate.postForObject(CARRY_URL+"/algo/pushnotification/client/" + clientData.getId(), pushNotifications, String.class);
    }
}
