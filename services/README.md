# Services 

### Build

mvn install

### API Documentation
http://api.carrycompanion.com/swagger-ui.html

### Deployment 
Micro Service Architecture With 3 Services All hosts on Kubernetes Cluster. 

Each of the below services are written in java using Spring Boot. 
For each service a docker image is created and then pushed to our Kubernetes Cluster. 
 
* API Service
* Algo Service
* Google Api Service 

