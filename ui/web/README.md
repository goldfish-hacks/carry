## UI - Web

### Build

1. Ensure you have the correct version of node(>13.0.1).   
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`node -v`
2. Install all of the depencies by running:  
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`npm install`
3. Run the local watch server by running:  
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`npm run start`
