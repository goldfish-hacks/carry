import React, { useEffect, useState } from "react";
import {
  IonContent,
  IonPage,
  IonSplitPane,
  IonMenu,
  IonTitle
} from "@ionic/react";

import { Header } from "../../components/Header";

import { ClientCard } from "../../components/ClientCard";
import { ClientChart } from "../../components/ClientChart";
import { RightPanel } from "../../components/RightPanel";
import { ALL_CLIENTS } from "../../constants/constants";

import "./Home.css";

interface ClientData {
  id: string;
  name: string;
  age: number;
  photo: string;
  appoinmentIn: string;
}

/**
 * Home page fot the therapist report view
 */
export const Home: React.FC = () => {
  //Initialize the clients array and get state affect
  const [clients, setClients] = useState<ClientData[]>([]);
  let [graphState, setGraph] = useState(false);

  // Fetch clients on mount
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(ALL_CLIENTS);
      const recivedClients = await response.json();
      setClients(recivedClients);
    }
    fetchData();
  }, []);

  return (
    <IonPage>
      <Header />
      <IonContent>
        <IonSplitPane contentId="menuContent">
          <IonMenu contentId="menuContent">
            <nav>
              <ul className="home__left-pane--no-bullets">
                <IonTitle className="home__left-pane-title" color="primary">
                  Upcoming Appointments
                </IonTitle>
                {clients.map((client: ClientData) => {
                  return (
                    <li className="home__left-pane-card" key={client.id}>
                      <ClientCard
                        name={client.name}
                        age={client.age}
                        appoinmentIn={client.appoinmentIn}
                        avatarUrl={client.photo}
                        onChange={() => setGraph(!graphState)}
                      />
                    </li>
                  );
                })}
              </ul>
            </nav>
          </IonMenu>
          <div className="home__main">
            {graphState && <ClientChart />}
            <div className="home_right-menu">
              <nav>
                {graphState && (
                  <RightPanel clientId="5dbcf3232c3cf60accea96ec" />
                )}
              </nav>
            </div>
          </div>
        </IonSplitPane>
      </IonContent>
    </IonPage>
  );
};
