import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  withIonLifeCycle,
  IonButton,
  IonFooter,
  IonList,
  IonItem,
  IonLabel,
  IonListHeader,
  IonText
} from "@ionic/react";
import React, { useState } from "react";

const NOTIFICATION_INIT_STATE = [
  { id: "id", title: "Test Push", body: "This is my first push notification" }
];

export const Reports: React.FC = () => {
  const [notifications, setNotifications] = useState<any[]>(
    NOTIFICATION_INIT_STATE
  );

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Enappd</IonTitle>
        </IonToolbar>
        <IonToolbar color="medium">
          <IonTitle>Ionic React Push Example</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonList>
          <IonListHeader>
            <IonLabel>Notifications</IonLabel>
          </IonListHeader>
          {notifications &&
            notifications.map((notif: any) => (
              <IonItem key={notif.id}>
                <IonLabel>
                  {notif.title}
                  {notif.body}
                </IonLabel>
              </IonItem>
            ))}
        </IonList>
      </IonContent>
      <IonFooter>
        <IonToolbar>
          <IonButton expand="full">Register for Push</IonButton>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  );
};
