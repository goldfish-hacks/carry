import React from "react";
import "hammerjs";
import {
  Chart,
  ChartTooltip,
  ChartTitle,
  ChartLegend,
  ChartSeries,
  ChartSeriesItem,
  ChartXAxis,
  ChartXAxisItem,
  ChartYAxis,
  ChartYAxisItem
} from "@progress/kendo-react-charts";

import "./ClientChart.css";

import data from "./data.json";

const yPlotBands = [
  {
    from: -5,
    to: 0,
    color: "#00f",
    opacity: 0.05
  }
];

export const ClientChart: React.FC = () => {
  return (
    <Chart>
      <ChartTitle text="" />
      <ChartSeries>
        <ChartSeriesItem
          type="bubble"
          xField="x"
          yField="y"
          sizeField="size"
          categoryField="category"
          colorField="medalColor"
          data={data}
        />
      </ChartSeries>
      <ChartXAxis>
        <ChartXAxisItem
          labels={{ format: "{0:N0}", skip: 1, rotation: "auto" }}
        />
      </ChartXAxis>
      <ChartYAxis>
        <ChartYAxisItem plotBands={yPlotBands} labels={{ format: "{0:N0}" }} />
      </ChartYAxis>
      <ChartLegend visible={true} />
      <ChartTooltip format="{3}: {2:N0}" opacity={3} />
    </Chart>
  );
};
