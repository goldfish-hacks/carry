import React from "react";
import { IonHeader, IonTitle, IonToolbar, IonSearchbar } from "@ionic/react";

import "./Header.css";

/**
 * Header component for the app
 */
export const Header: React.FC = () => {
  return (
    <IonHeader>
      <IonToolbar>
        <div className="header__items-container">
          <IonTitle className="header__title">
            <img
              className="header__image"
              src="../assets/icon/favicon.png"
              alt="Logo"
            />
          </IonTitle>
          <IonSearchbar
            className="header__search"
            placeholder="Search Clients"
          ></IonSearchbar>
        </div>
      </IonToolbar>
    </IonHeader>
  );
};
