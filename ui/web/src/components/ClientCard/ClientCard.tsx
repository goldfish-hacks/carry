import React from "react";
import {
  IonCard,
  IonCardContent,
  IonContent,
  IonAvatar,
  IonGrid,
  IonRow,
  IonCol,
  IonLabel
} from "@ionic/react";
import "./ClientCard.css";

/**
 * Props for the client card componet.
 */
export interface Props {
  /**
   * The url for the clients avatar (ie. thier photo).
   */
  avatarUrl: string;

  /**
   * The clients name.
   */
  name: string;

  /**
   * The clients age.
   */
  age: number;

  /**
   * Represent how much time is left until the clients appoiment.
   */
  appoinmentIn: string;

  /**
   * Function to trigger change
   */
  onChange: () => void;
}

/**
 * Generic client card component to render the deatils of the client.
 */
export const ClientCard: React.FC<Props> = ({
  name,
  avatarUrl,
  age,
  onChange
}) => {
  return (
    <IonContent>
      <IonCard onClick={() => onChange()}>
        <IonCardContent>
          <IonGrid>
            <IonRow className="client-card__row">
              <IonCol size="3">
                <IonAvatar>
                  <img src={avatarUrl} alt="img not found" />
                </IonAvatar>
              </IonCol>
              <IonCol size="9">
                <div className="client-card__col">
                  <IonLabel>
                    <b>
                      <h2>{name}</h2>
                    </b>
                  </IonLabel>
                  <IonLabel>
                    <b>Age</b> {Math.trunc(age)}
                  </IonLabel>
                  <IonLabel>
                    <b>See In</b> {`1 Day`}
                  </IonLabel>
                </div>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonCardContent>
      </IonCard>
    </IonContent>
  );
};
