import React, { useState, useEffect } from "react";
import { IonList, IonItem, IonTitle } from "@ionic/react";

import { CLIENT_BASE } from "../../constants/constants";

import "./RightPanel.css";

export interface Props {
  clientId: string;
}

/**
 * Right Panel component for the app
 */
export const RightPanel: React.FC<Props> = ({ clientId }) => {
  //Initialize the clients array and get state affect
  const [insights, setInsights] = useState([]);

  // Fetch clients on mount
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(`${CLIENT_BASE}/${clientId}/insight`);
      const recivedInsights = await response.json();
      setInsights(recivedInsights);
    }
    fetchData();
  }, [clientId]);

  return (
    <div>
      <IonTitle color="primary" className="right-panel__title--padding">
        Insights
      </IonTitle>
      <IonList>
        {insights.map((insight: any) => {
          return (
            <IonItem text-wrap key={insight.name}>
              {insight.data}
            </IonItem>
          );
        })}
      </IonList>
    </div>
  );
};
