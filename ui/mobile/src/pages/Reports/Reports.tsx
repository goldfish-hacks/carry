import {
  IonContent,
  IonPage,
  IonList,
  IonItem,
  IonLabel,
  IonListHeader
} from "@ionic/react";
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed
} from "@capacitor/core";
import React, { useState } from "react";
import { Header } from "../../components/Header";
import { ActionFooter } from "../../components/ActionFooter";

const { PushNotifications } = Plugins;
const NOTIFICATION_INIT_STATE = [
  {
    id: "id",
    title: "Mindset Check In",
    body: "How is your mindset right now?"
  }
];

export const Reports: React.FC = () => {
  const [notifications, setNotifications] = useState<any[]>(
    NOTIFICATION_INIT_STATE
  );

  const push = () => {
    // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();

    // On success, we should be able to receive notifications
    PushNotifications.addListener(
      "registration",
      (token: PushNotificationToken) => {
        alert("Push registration success, token: " + token.value);
      }
    );

    // Some issue with your setup and push will not work
    PushNotifications.addListener("registrationError", (error: any) => {
      alert("Error on registration: " + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      "pushNotificationReceived",
      (notification: PushNotification) => {
        let notif = notifications;
        notif.push({
          id: notification.id,
          title: notification.title,
          body: notification.body
        });
        setNotifications(notif);
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      "pushNotificationActionPerformed",
      (notification: PushNotificationActionPerformed) => {
        let notif = notifications;
        notif.push({
          id: notification.notification.data.id,
          title: notification.notification.data.title,
          body: notification.notification.data.body
        });
        setNotifications(notif);
      }
    );
  };

  return (
    <IonPage>
      <Header />
      <IonContent className="ion-padding">
        <IonList>
          <IonListHeader>
            <IonLabel>Notifications</IonLabel>
          </IonListHeader>
          {notifications &&
            notifications.map((notif: any) => (
              <IonItem key={notif.id}>
                <IonLabel>
                  <h3>{notif.title}</h3>
                  <p>{notif.body}</p>
                </IonLabel>
              </IonItem>
            ))}
        </IonList>
      </IonContent>
      <ActionFooter text="Register" isDisabled={false} handleSave={push} />
    </IonPage>
  );
};
