import {
  IonContent,
  IonLabel,
  IonSegment,
  IonSegmentButton,
  IonIcon,
  IonChip,
  IonNote,
  IonRefresher,
  IonRefresherContent,
  IonTextarea
} from "@ionic/react";
import { RefresherEventDetail } from "@ionic/core";
import { happy, sad, checkmark } from "ionicons/icons";
import React, { useState } from "react";
import classnames from "classnames";

import { Header } from "../../components/Header";
import { ActionFooter } from "../../components/ActionFooter";

import "./Home.css";

const POSITIVE_EMOTIONS: string[] = [
  "Joy",
  "Gratitude",
  "Serenity",
  "Interest",
  "Hope",
  "Pride",
  "Amusement",
  "Inspiration",
  "Awe",
  "Relief",
  "Affection",
  "Surprise",
  "Confidence",
  "Admiration"
];

const NEGATIVE_EMOTIONS: string[] = [
  "Anger",
  "Annoyance",
  "Sadness",
  "Guilt",
  "Fear",
  "Anxiety",
  "Discouragement",
  "Despair",
  "Apathy",
  "Disappointment",
  "Frustration"
];

export const Home: React.FC = () => {
  const [selectedMindset, setSelectedMindset] = useState("");
  const [selectedEmotions, setSelectedEmotions] = useState<string[]>([]);
  const [isSaving, setIsSaving] = useState(false);
  const [showConfirmation, setShowConfirmation] = useState(false);

  const resetInputs = () => {
    setSelectedMindset("");
    setSelectedEmotions([]);
  };

  const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
    setTimeout(() => {
      window.location.reload(true);
      event.detail.complete();
    }, 1000);
  };

  const handleEmotionSelection = (emotion: string) => {
    const selectedEmotionsCopy = [...selectedEmotions];
    const idx = selectedEmotionsCopy.indexOf(emotion);
    if (idx > -1) {
      selectedEmotionsCopy.splice(idx, 1);
    } else {
      selectedEmotionsCopy.push(emotion);
    }
    setSelectedEmotions(selectedEmotionsCopy);
  };

  const handleSave = () => {
    setShowConfirmation(true);
    setIsSaving(true);
    resetInputs();
    setTimeout(() => {
      setIsSaving(false);
      setTimeout(() => {
        setShowConfirmation(false);
      }, 1500);
    }, 2000);
  };

  return (
    <>
      <Header />

      <IonContent className="home">
        <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
          <IonRefresherContent />
        </IonRefresher>

        <div
          className={classnames({
            "home--hidden": !showConfirmation,
            "home--shown": showConfirmation,
            home__confirmation: showConfirmation
          })}
        >
          <div
            className={classnames("home__confirmation-circle-loader", {
              "home__confirmation-circle-loader--load-complete": !isSaving
            })}
          >
            <IonIcon
              className={classnames({
                "home__confirmation-icon": !isSaving,
                "home--hidden": isSaving,
                "home--shown": !isSaving
              })}
              icon={checkmark}
              color="success"
            />
          </div>
        </div>

        <div
          className={classnames({
            "home--hidden": showConfirmation,
            "home--shown": !showConfirmation
          })}
        >
          <h1 className="home__title">How is your mindset right now?</h1>
          <IonSegment onIonChange={e => setSelectedMindset(e.detail.value!)}>
            <IonSegmentButton
              value="positive"
              className="home__mindset--positive"
            >
              <IonIcon icon={happy} />
              <IonLabel>Positive</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton
              value="negative"
              className="home__mindset--negative"
            >
              <IonIcon icon={sad} />
              <IonLabel>Negative</IonLabel>
            </IonSegmentButton>
          </IonSegment>

          <div
            className={classnames({
              "home--hidden": !selectedMindset,
              "home--shown": selectedMindset
            })}
          >
            <div className="home__followup-title">
              <h3>Can you tell me more?</h3>
              <IonNote>Optional</IonNote>
            </div>

            {(selectedMindset === "positive"
              ? POSITIVE_EMOTIONS
              : NEGATIVE_EMOTIONS
            ).map((emotion: string) => (
              <IonChip
                outline={!selectedEmotions.includes(emotion)}
                color="primary"
                key={emotion}
                onClick={() => handleEmotionSelection(emotion)}
              >
                <IonLabel>{emotion}</IonLabel>
              </IonChip>
            ))}

            <div className="home__followup-thoughts">
              <IonTextarea placeholder="Record any other thoughts..." />
            </div>
          </div>
        </div>
      </IonContent>

      <div
        className={classnames({
          "home--hidden": showConfirmation,
          "home--shown": !showConfirmation
        })}
      >
        <ActionFooter
          text="Carry On"
          isDisabled={selectedMindset === ""}
          handleSave={handleSave}
        />
      </div>
    </>
  );
};
