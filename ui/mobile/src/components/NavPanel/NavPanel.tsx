import {
  IonContent,
  IonHeader,
  IonItem,
  IonIcon,
  IonLabel,
  IonMenu,
  IonList,
  IonThumbnail
} from "@ionic/react";
import React from "react";
import { happy, today } from "ionicons/icons";

import "./NavPanel.css";

export const NavPanel: React.FC = () => {
  return (
    <>
      <IonMenu contentId="main">
        <IonHeader className="nav-panel__header">
          <IonThumbnail className="nav-panel__header-img">
            <img src="../assets/headshot.jpg" alt="Profile" />
          </IonThumbnail>
          <h4>Jane Smith</h4>
          <div className="nav-panel__header-subtext">Reflections Group</div>
        </IonHeader>
        <IonContent>
          <IonList>
            <IonItem routerLink="/">
              <IonIcon icon={happy} color="primary" />
              <IonLabel className="nav-panel__item-label">Mindset</IonLabel>
            </IonItem>
            <IonItem routerLink="/reports">
              <IonIcon icon={today} color="primary" />
              <IonLabel className="nav-panel__item-label">Reports</IonLabel>
            </IonItem>
          </IonList>
        </IonContent>
      </IonMenu>
    </>
  );
};
