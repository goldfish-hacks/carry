import { IonFooter, IonButton } from "@ionic/react";
import React from "react";

import "./ActionFooter.css";

export interface ActionFooterProps {
  text: string;
  isDisabled: boolean;
  handleSave: () => void;
}

export const ActionFooter: React.FC<ActionFooterProps> = (
  props: ActionFooterProps
) => {
  const { text, isDisabled, handleSave } = props;
  return (
    <IonFooter>
      <IonButton
        className="action-footer"
        shape="round"
        expand="block"
        disabled={isDisabled}
        onClick={handleSave}
      >
        {text}
      </IonButton>
    </IonFooter>
  );
};
