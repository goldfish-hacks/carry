import {
  IonToolbar,
  IonHeader,
  IonButtons,
  IonMenuButton,
  IonTitle
} from "@ionic/react";
import React from "react";

import "./Header.css";

export const Header: React.FC = () => {
  return (
    <IonHeader>
      <IonToolbar className="header">
        <IonButtons slot="start">
          <IonMenuButton></IonMenuButton>
        </IonButtons>
        <IonTitle>
          <img
            className="header__logo"
            src="../assets/icon/favicon.png"
            alt="Logo"
          />
        </IonTitle>
      </IonToolbar>
    </IonHeader>
  );
};
