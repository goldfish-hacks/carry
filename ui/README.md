## UI

<a href="https://nodejs.org/en/" alt="Node">
        <img src="https://img.shields.io/badge/node-13.0.1-brightgreen" /></a>    
<a href="https://www.npmjs.com/" alt="Npm">
        <img src="https://img.shields.io/badge/npm-6.12.0-brightgreen" /></a>
<a href="https://github.com/prettier/prettier" alt="Node">
        <img src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat" /></a>
<br />

Both our therapist and the companion client app were built using React/Typescript and the [Ionic](https://ionicframework.com/docs/) framework.

[Prettier](https://prettier.io/) used for code formatting.

[BEM](http://getbem.com/naming/) used for CSS naming convention.

### Prerequisites

1. node.js > 13.0.1
2. ionic > 5.4.5

### Therapist App (web)

This is the main web app for Cognitive Behavioral Therapists to get insight into their clients' thought and behavior data.

Please see the local [README](./web/README.md) for more info.

### Client App (mobile)

This is the main mobile app for the patients to complete thier activities and report on daily mood patters.

Please see the local [README](./mobile/README.md) for more info.
