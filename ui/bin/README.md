# Simple Express Server

This is a simple express server to host the static files for the carry's mobile/web assets.

### Usage

1. The simple server expects the mobile/web server assets to be two directories above (ie. `../../`) please ensure you copy the built files to the directories `web` and `mobile` respectively.
2. Install all of the dependencies by running `npm install`.
3. Run the simple server by running `npm start`.
