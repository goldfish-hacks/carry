/**
 * Simple express sever
 * 
 * Express: http://expressjs.com
 */
var path = require('path'),
    express = require('express'),
    host = process.env.HOST || '0.0.0.0',
    port1 = process.env.PORT || 8100,
    port2 = process.env.PORT || 8000,
    root = path.resolve(__dirname, '..');
 
app1 = express();
app1.use(function(req, res, next) { console.log(req.url); next(); });
app1.use(express.static(root + '../../mobile/'));
server1 = app1.listen(port1, host, serverStarted(host, port1));

app2 = express();
app2.use(function(req, res, next) { console.log(req.url); next(); });
app2.use(express.static(root + '../../web/'));
server2 = app2.listen(port2, host, serverStarted(host, port2));
 
function serverStarted (host, port) {
    console.log('Server started', host, port);
}
